import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsulterPage } from './consulter.page';

const routes: Routes = [
  {
    path: '',
    component: ConsulterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsulterPageRoutingModule {}
