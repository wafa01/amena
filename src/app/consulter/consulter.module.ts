import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsulterPageRoutingModule } from './consulter-routing.module';

import { ConsulterPage } from './consulter.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsulterPageRoutingModule
  ],
  declarations: [ConsulterPage]
})
export class ConsulterPageModule {}
